<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	$meta = array(
		array(
				'name' => 'robots',
				'content' => 'no-cache'
		),
		array(
				'name' => 'description',
				'content' => 'My Great Generic application'
		),
		array(
				'name' => 'keywords',
				'content' => 'view, navigation, form, query'
		),
		array(
				'name' => 'robots',
				'content' => 'no-cache'
		),
		array(
				'name' => 'Content-type',
				'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
		)
	);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Generic Application Administrator Developer Dashboard For Any Platform</title>
		
		<?=meta($meta)?>
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		<!-- vector map CSS -->
		<?php echo link_tag('resource/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css'); ?>
		<!-- Custom CSS -->
		<?php echo link_tag('resource/dist/css/style.css'); ?>
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="index.html">
						<img class="brand-img mr-10" src="<?php echo base_url(); ?>resource/img/logo.png" alt="brand" style="height:50px"/>
						<img class="brand-img mr-10" src="<?php echo base_url(); ?>resource/img/logo_two.jpg" alt="brand" style="height:50px"/>
						<img class="brand-img mr-10" src="<?php echo base_url(); ?>resource/img/logo_one.png" alt="brand" style="height:50px"/>
						<!--span class="brand-text">Jetson</span-->
					</a>
				</div>
				<!--div class="form-group mb-0 pull-right">
					<span class="inline-block pr-10">Don't have an account?</span>
					<a class="inline-block btn btn-info  btn-rounded btn-outline" href="signup.html">Sign Up</a>
				</div-->
				<div class="clearfix"></div>
			</header>
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Generic Application </h3>
											<h6 class="text-center nonecase-font txt-grey">Technician Developer Dashboard</h6>
											<span class="text-center nonecase-font txt-danger" ><?=validation_errors()?></span>
											<span class="text-center nonecase-font txt-danger" ><?=$this->session->flashdata('error')?></span>
										</div>	
										<div class="form-wrap">
											<?php echo form_open_multipart(); ?>
												<div class="form-group">
													<label class="control-label mb-10" for="email">Email address</label>
													<input name="email" type="email" class="form-control" required="" id="email" placeholder="Enter email">
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="password">Password</label>
													<a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="#!">forgot password ?</a>
													<div class="clearfix"></div>
													<input name="password" type="password" class="form-control" required="" id="password" placeholder="Enter Password">
												</div>
												
												<div class="form-group">
													<div class="checkbox checkbox-primary pr-10 pull-left">
														<input id="checkbox_2" required="" type="checkbox">
														<label for="checkbox_2"> Keep me logged in</label>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="form-group text-center">
													<button type="submit" class="btn  btn-primary btn-outline">sign in</button>
												</div>
											<?php echo form_close(); ?>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
		<script src="<?php echo base_url(); ?>resource/vendors/bower_components/jquery/dist/jquery.min.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="<?php echo base_url(); ?>resource/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>resource/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
		
		<!-- Slimscroll JavaScript -->
		<script src="<?php echo base_url(); ?>resource/dist/js/jquery.slimscroll.js"></script>
		
		<!-- Init JavaScript -->
		<script src="<?php echo base_url(); ?>resource/dist/js/init.js"></script>
	</body>

</html>