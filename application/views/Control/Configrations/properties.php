				<div class="container-fluid">
					<!-- Title -->
					<div class="row heading-bg">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						  <h5 class="txt-dark"> Configrations </h5>
						</div>
						<!-- Breadcrumb -->
						<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						  <ol class="breadcrumb">
							<li><a href="index.html">Dashboard</a></li>
							<li><a href="#"><span>Configrations</span></a></li>
							<li class="active"><span> Properties </span></li>
						  </ol>
						</div>
						<!-- /Breadcrumb -->
					</div>
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-sm-12 ol-md-12 col-xs-12">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark"> Company Properties </h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<p class="text-muted" > <!-- more description to app --></p>
										<div class="mt-40">
											<?=form_open_multipart()?>
												<?=form_input_selector(
													"text" , 
													(object) array("lable" => "Company name") ,
													extract_lable( !isset($properties->data) ? array() : json_decode($properties->data)  , "Company name")
												)?>
												
												<?=form_input_selector(
													"text" , 
													(object) array("lable" => "company meta description") ,
													extract_lable( !isset($properties->data) ? array() : json_decode($properties->data)  , "company meta description")
												)?>
												
												<?=form_input_selector(
													"text" , 
													(object) array("lable" => "company meta title") ,
													extract_lable( !isset($properties->data) ? array() : json_decode($properties->data)  , "company meta title")
												)?>
												
												<?=form_input_selector(
													"text" , 
													(object) array("lable" => "company meta keywords") ,
													extract_lable( !isset($properties->data) ? array() : json_decode($properties->data)  , "company meta keywords")
												)?>
												
												<?=d_submit(0)?>
											<?=form_close()?>
										</div>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

				</div>