<style>
.dropify-wrapper {
    height: 800px !important;
}
</style>
				<div class="container-fluid">
					<!-- Title -->
					<div class="row heading-bg">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						  <h5 class="txt-dark"> Configrations </h5>
						</div>
						<!-- Breadcrumb -->
						<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						  <ol class="breadcrumb">
							<li><a href="index.html">Dashboard</a></li>
							<li><a href="#"><span>Configrations</span></a></li>
							<li class="active"><span>Login Background</span></li>
						  </ol>
						</div>
						<!-- /Breadcrumb -->
					</div>
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-sm-12 ol-md-12 col-xs-12">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark"> Login Background </h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<p class="text-muted" > <!--You can set a default value by adding <code>data-default-file</code> in input tag. --></p>
										<div class="mt-40">
											<?=form_open_multipart()?>
												<?=d_submit(0)?>
												<input type="file" name="login_wallpaper_image" id="input-file-now-custom-1" class="dropify" 
													<?php if($login_wallpaper){ ?>
														data-default-file="<?php echo base_url(json_decode($login_wallpaper->data)->login_wallpaper_image); ?>" 
													<?php } ?>
												/>
											<?=form_close()?>
										</div>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

				</div>
					<!-- /Row -->
