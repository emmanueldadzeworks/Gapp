<div class="container-fluid">
					<!-- Title -->
					<div class="row heading-bg">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						  <h5 class="txt-dark"> Configrations </h5>
						</div>
						<!-- Breadcrumb -->
						<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						  <ol class="breadcrumb">
							<li><a href="index.html">Dashboard</a></li>
							<li><a href="#"><span>Configrations</span></a></li>
							<li class="active"><span> Properties </span></li>
						  </ol>
						</div>
						<!-- /Breadcrumb -->
					</div>
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-sm-12 ol-md-12 col-xs-12">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark"> Company Properties </h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<p class="text-muted" > <!-- more description to app --></p>
										<div class="mt-40">
											<?=form_open_multipart()?>
                                                <div class="row">
                                                    <!-- .col -->
                                                    <div class="col-md-7 col-sm-12 col-xs-12">
                                                        <div class="white-box">
                                                            <h3 class="box-title">Yearly Sales</h3>
                                                            <ul class="list-inline text-right">
                                                                <li>
                                                                    <h5><i class="fa fa-circle m-r-5" style="color: #00b5c2;"></i>iPhone</h5>
                                                                </li>
                                                                <li>
                                                                    <h5><i class="fa fa-circle m-r-5" style="color: #f75b36;"></i>iPad</h5>
                                                                </li>
                                                                <li>
                                                                    <h5><i class="fa fa-circle m-r-5" style="color: #2c5ca9;"></i>iPod</h5>
                                                                </li>
                                                            </ul>
                                                            <div id="morris-area-chart" style="height: 356px; position: relative;"><svg height="356" version="1.1" width="619" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; top: -0.399994px;"><desc>Created with Raphaël 2.1.2</desc><defs></defs><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="35" y="316.5" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">0</tspan></text><path style="" fill="none" stroke="#e0e0e0" d="M47.5,316.5H594" stroke-width="0.5"></path><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="35" y="243.625" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">75</tspan></text><path style="" fill="none" stroke="#e0e0e0" d="M47.5,243.625H594" stroke-width="0.5"></path><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="35" y="170.75" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">150</tspan></text><path style="" fill="none" stroke="#e0e0e0" d="M47.5,170.75H594" stroke-width="0.5"></path><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="35" y="97.875" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">225</tspan></text><path style="" fill="none" stroke="#e0e0e0" d="M47.5,97.875H594" stroke-width="0.5"></path><text style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="35" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal"><tspan dy="4">300</tspan></text><path style="" fill="none" stroke="#e0e0e0" d="M47.5,25H594" stroke-width="0.5"></path><text style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="594" y="329" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal" transform="matrix(1,0,0,1,0,7.25)"><tspan dy="4">2016</tspan></text><text style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="502.9582382473757" y="329" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal" transform="matrix(1,0,0,1,0,7.25)"><tspan dy="4">2015</tspan></text><text style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="411.9164764947513" y="329" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal" transform="matrix(1,0,0,1,0,7.25)"><tspan dy="4">2014</tspan></text><text style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="320.8747147421269" y="329" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal" transform="matrix(1,0,0,1,0,7.25)"><tspan dy="4">2013</tspan></text><text style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="229.58352350524876" y="329" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal" transform="matrix(1,0,0,1,0,7.25)"><tspan dy="4">2012</tspan></text><text style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="138.54176175262438" y="329" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal" transform="matrix(1,0,0,1,0,7.25)"><tspan dy="4">2011</tspan></text><text style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" x="47.5" y="329" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" font-weight="normal" transform="matrix(1,0,0,1,0,7.25)"><tspan dy="4">2010</tspan></text><path style="fill-opacity: 0.6;" fill="#f28e76" stroke="none" d="M47.5,316.5C70.2604404381561,284.92083333333335,115.78132131446829,199.9,138.54176175262438,190.18333333333334C161.30220219078046,180.46666666666667,206.82308306709268,231.4891358869129,229.58352350524876,238.76666666666665C252.4063213144683,246.06413588691288,298.0519169329074,260.6457820337437,320.8747147421269,248.48333333333335C343.635155180283,236.35411536707707,389.1560360565952,145.85104166666667,411.9164764947513,141.6C434.6769169329074,137.34895833333331,480.1977978092196,222.97708333333333,502.9582382473757,214.475C525.7186786855318,205.97291666666666,571.2395595618439,108.80625,594,73.58333333333334L594,316.5L47.5,316.5Z" fill-opacity="0.6"></path><path style="" fill="none" stroke="#f75b36" d="M47.5,316.5C70.2604404381561,284.92083333333335,115.78132131446829,199.9,138.54176175262438,190.18333333333334C161.30220219078046,180.46666666666667,206.82308306709268,231.4891358869129,229.58352350524876,238.76666666666665C252.4063213144683,246.06413588691288,298.0519169329074,260.6457820337437,320.8747147421269,248.48333333333335C343.635155180283,236.35411536707707,389.1560360565952,145.85104166666667,411.9164764947513,141.6C434.6769169329074,137.34895833333331,480.1977978092196,222.97708333333333,502.9582382473757,214.475C525.7186786855318,205.97291666666666,571.2395595618439,108.80625,594,73.58333333333334" stroke-width="0"></path><circle cx="47.5" cy="316.5" r="0" fill="#f75b36" stroke="#f75b36" style="" stroke-width="1"></circle><circle cx="138.54176175262438" cy="190.18333333333334" r="0" fill="#f75b36" stroke="#f75b36" style="" stroke-width="1"></circle><circle cx="229.58352350524876" cy="238.76666666666665" r="0" fill="#f75b36" stroke="#f75b36" style="" stroke-width="1"></circle><circle cx="320.8747147421269" cy="248.48333333333335" r="0" fill="#f75b36" stroke="#f75b36" style="" stroke-width="1"></circle><circle cx="411.9164764947513" cy="141.6" r="0" fill="#f75b36" stroke="#f75b36" style="" stroke-width="1"></circle><circle cx="502.9582382473757" cy="214.475" r="0" fill="#f75b36" stroke="#f75b36" style="" stroke-width="1"></circle><circle cx="594" cy="73.58333333333334" r="0" fill="#f75b36" stroke="#f75b36" style="" stroke-width="1"></circle><path style="fill-opacity: 0.6;" fill="#0bcfdd" stroke="none" d="M47.5,316.5C70.2604404381561,292.2083333333333,115.78132131446829,226.6208333333333,138.54176175262438,219.33333333333331C161.30220219078046,212.04583333333332,206.82308306709268,270.32921796625624,229.58352350524876,258.2C252.4063213144683,246.0375512995896,298.0519169329074,133.112870497036,320.8747147421269,122.16666666666666C343.635155180283,111.25037049703602,389.1560360565952,158.60416666666666,411.9164764947513,170.75C434.6769169329074,182.89583333333334,480.1977978092196,219.33333333333331,502.9582382473757,219.33333333333331C525.7186786855318,219.33333333333331,571.2395595618439,182.89583333333331,594,170.75L594,316.5L47.5,316.5Z" fill-opacity="0.6"></path><path style="" fill="none" stroke="#00b5c2" d="M47.5,316.5C70.2604404381561,292.2083333333333,115.78132131446829,226.6208333333333,138.54176175262438,219.33333333333331C161.30220219078046,212.04583333333332,206.82308306709268,270.32921796625624,229.58352350524876,258.2C252.4063213144683,246.0375512995896,298.0519169329074,133.112870497036,320.8747147421269,122.16666666666666C343.635155180283,111.25037049703602,389.1560360565952,158.60416666666666,411.9164764947513,170.75C434.6769169329074,182.89583333333334,480.1977978092196,219.33333333333331,502.9582382473757,219.33333333333331C525.7186786855318,219.33333333333331,571.2395595618439,182.89583333333331,594,170.75" stroke-width="0"></path><circle cx="47.5" cy="316.5" r="0" fill="#00b5c2" stroke="#00b5c2" style="" stroke-width="1"></circle><circle cx="138.54176175262438" cy="219.33333333333331" r="0" fill="#00b5c2" stroke="#00b5c2" style="" stroke-width="1"></circle><circle cx="229.58352350524876" cy="258.2" r="0" fill="#00b5c2" stroke="#00b5c2" style="" stroke-width="1"></circle><circle cx="320.8747147421269" cy="122.16666666666666" r="0" fill="#00b5c2" stroke="#00b5c2" style="" stroke-width="1"></circle><circle cx="411.9164764947513" cy="170.75" r="0" fill="#00b5c2" stroke="#00b5c2" style="" stroke-width="1"></circle><circle cx="502.9582382473757" cy="219.33333333333331" r="0" fill="#00b5c2" stroke="#00b5c2" style="" stroke-width="1"></circle><circle cx="594" cy="170.75" r="0" fill="#00b5c2" stroke="#00b5c2" style="" stroke-width="1"></circle><path style="fill-opacity: 0.6;" fill="#37a2f4" stroke="none" d="M47.5,316.5C70.2604404381561,297.06666666666666,115.78132131446829,247.26874999999998,138.54176175262438,238.76666666666665C161.30220219078046,230.26458333333332,206.82308306709268,258.1867077063384,229.58352350524876,248.48333333333335C252.4063213144683,238.75337437300504,298.0519169329074,167.11455768353852,320.8747147421269,161.03333333333333C343.635155180283,154.9687243502052,389.1560360565952,191.39791666666667,411.9164764947513,199.9C434.6769169329074,208.40208333333334,480.1977978092196,238.76666666666668,502.9582382473757,229.05C525.7186786855318,219.33333333333334,571.2395595618439,148.8875,594,122.16666666666666L594,316.5L47.5,316.5Z" fill-opacity="0.6"></path><path style="" fill="none" stroke="#008efa" d="M47.5,316.5C70.2604404381561,297.06666666666666,115.78132131446829,247.26874999999998,138.54176175262438,238.76666666666665C161.30220219078046,230.26458333333332,206.82308306709268,258.1867077063384,229.58352350524876,248.48333333333335C252.4063213144683,238.75337437300504,298.0519169329074,167.11455768353852,320.8747147421269,161.03333333333333C343.635155180283,154.9687243502052,389.1560360565952,191.39791666666667,411.9164764947513,199.9C434.6769169329074,208.40208333333334,480.1977978092196,238.76666666666668,502.9582382473757,229.05C525.7186786855318,219.33333333333334,571.2395595618439,148.8875,594,122.16666666666666" stroke-width="0"></path><circle cx="47.5" cy="316.5" r="0" fill="#008efa" stroke="#008efa" style="" stroke-width="1"></circle><circle cx="138.54176175262438" cy="238.76666666666665" r="0" fill="#008efa" stroke="#008efa" style="" stroke-width="1"></circle><circle cx="229.58352350524876" cy="248.48333333333335" r="0" fill="#008efa" stroke="#008efa" style="" stroke-width="1"></circle><circle cx="320.8747147421269" cy="161.03333333333333" r="0" fill="#008efa" stroke="#008efa" style="" stroke-width="1"></circle><circle cx="411.9164764947513" cy="199.9" r="0" fill="#008efa" stroke="#008efa" style="" stroke-width="1"></circle><circle cx="502.9582382473757" cy="229.05" r="0" fill="#008efa" stroke="#008efa" style="" stroke-width="1"></circle><circle cx="594" cy="122.16666666666666" r="0" fill="#008efa" stroke="#008efa" style="" stroke-width="1"></circle></svg><div class="morris-hover morris-default-style" style="left: 493px; top: 49px; display: none;"><div class="morris-hover-row-label">2016</div><div class="morris-hover-point" style="color: #f75b36">
                                                                iPhone:
                                                                250
                                                                </div><div class="morris-hover-point" style="color: #00b5c2 ">
                                                                iPad:
                                                                150
                                                                </div><div class="morris-hover-point" style="color: #008efa">
                                                                iPod Touch:
                                                                200
                                                                </div></div></div>
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col-md-5 col-sm-6">
                                                        <div class="row">
                                                            <!-- .col -->
                                                            <div class="col-md-6 col-sm-12">
                                                                <div class="white-box text-center bg-megna">
                                                                    <h1 class="text-white counter">165</h1>
                                                                    <p class="text-white">New items</p>
                                                                </div>
                                                            </div>
                                                            <!-- /.col -->
                                                            <!-- .col -->
                                                            <div class="col-md-6 col-sm-12">
                                                                <div class="white-box text-center bg-inverse">
                                                                    <h1 class="text-white counter">2065</h1>
                                                                    <p class="text-white">Feeds</p>
                                                                </div>
                                                            </div>
                                                            <!-- /.col -->
                                                            <!-- .col -->
                                                            <div class="col-md-6 col-sm-12">
                                                                <div class="white-box text-center bg-info">
                                                                    <h1 class="counter text-white">465</h1>
                                                                    <p class="text-white">Reviews</p>
                                                                </div>
                                                            </div>
                                                            <!-- /.col -->
                                                            <!-- .col -->
                                                            <div class="col-md-6 col-sm-12">
                                                                <div class="white-box text-center bg-danger">
                                                                    <h1 class="text-white counter">6555</h1>
                                                                    <p class="text-white">Customers</p>
                                                                </div>
                                                            </div>
                                                            <!-- /.col -->
                                                            <!-- .col -->
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="white-box">
                                                                    <h3 class="box-title">Revenue of the site</h3>
                                                                    <ul class="list-inline two-part">
                                                                        <li><i class="icon-folder-alt text-danger"></i></li>
                                                                        <li class="text-right"><span class="counter">36411</span></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <!-- /.col -->
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4 col-lg-3 col-sm-6 col-xs-12">
                                                        <div class="white-box m-b-15">
                                                            <h3 class="box-title">Tax Deduction</h3>
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-6  m-t-30">
                                                                    <h1 class="text-info">$647</h1>
                                                                    <p class="text-muted">APRIL 2016</p>
                                                                    <b>(150 Sales)</b>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                    <div id="sparkline2dash" class="text-center"><canvas style="display: inline-block; width: 88px; height: 154px; vertical-align: top;" width="88" height="154"></canvas></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-lg-3 col-sm-6 col-xs-12">
                                                        <div class="white-box m-b-15">
                                                            <h3 class="box-title">Shipment</h3>
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-6  m-t-30">
                                                                    <h1 class="text-megna">$347</h1>
                                                                    <p class="text-muted">APRIL 2016</p>
                                                                    <b class="">(150 Sales)</b>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                    <div id="sales1" class="text-center"><canvas style="display: inline-block; width: 120px; height: 120px; vertical-align: top;" width="120" height="120"></canvas></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-lg-3 col-sm-6 col-xs-12">
                                                        <div class="white-box m-b-15">
                                                            <h3 class="box-title">Revenue Generate</h3>
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-6  m-t-30">
                                                                    <h1 class="text-danger">$647</h1>
                                                                    <p class="text-muted">APRIL 2016</p>
                                                                    <b>(150 Sales)</b>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                    <div id="sparkline2dash2" class="text-center"><canvas style="display: inline-block; width: 88px; height: 154px; vertical-align: top;" width="88" height="154"></canvas></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-lg-3 col-sm-6 col-xs-12">
                                                        <div class="white-box m-b-15">
                                                            <h3 class=" box-title">Order Got</h3>
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-6  m-t-30">
                                                                    <h1 class="">$347</h1>
                                                                    <p class="text-muted">APRIL 2016</p>
                                                                    <b class="">(150 Sales)</b>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                    <div id="sales12" class="text-center"><canvas style="display: inline-block; width: 88px; height: 154px; vertical-align: top;" width="88" height="154"></canvas></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4 col-xs-12 col-sm-6">
                                                        <div class="white-box">
                                                            <h3 class="box-title">To Do List</h3>
                                                            <ul class="list-task list-group" data-role="tasklist">
                                                                <li class="list-group-item" data-role="task">
                                                                    <div class="checkbox checkbox-info">
                                                                        <input id="inputSchedule" name="inputCheckboxesSchedule" type="checkbox">
                                                                        <label for="inputSchedule"> <span>Schedule meeting</span> </label>
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item" data-role="task">
                                                                    <div class="checkbox checkbox-info">
                                                                        <input id="inputCall" name="inputCheckboxesCall" type="checkbox">
                                                                        <label for="inputCall"> <span>Give Purchase report</span> <span class="label label-danger">Today</span> </label>
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item" data-role="task">
                                                                    <div class="checkbox checkbox-info">
                                                                        <input id="inputBook" name="inputCheckboxesBook" type="checkbox">
                                                                        <label for="inputBook"> <span>Book flight for holiday</span> </label>
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item" data-role="task">
                                                                    <div class="checkbox checkbox-info">
                                                                        <input id="inputForward" name="inputCheckboxesForward" type="checkbox">
                                                                        <label for="inputForward"> <span>Forward all tasks</span> <span class="label label-warning">2 weeks</span> </label>
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item" data-role="task">
                                                                    <div class="checkbox checkbox-info">
                                                                        <input id="inputRecieve" name="inputCheckboxesRecieve" type="checkbox">
                                                                        <label for="inputRecieve"> <span>Recieve shipment</span> </label>
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item" data-role="task">
                                                                    <div class="checkbox checkbox-info">
                                                                        <input id="inputForward2" name="inputCheckboxesd" type="checkbox">
                                                                        <label for="inputForward2"> <span>Important tasks</span> <span class="label label-success">2 weeks</span> </label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-xs-12 col-sm-6">
                                                        <div class="white-box">
                                                            <h3 class="box-title">You have 5 new messages</h3>
                                                            <div class="message-center">
                                                                <a href="#">
                                                                    <div class="user-img"> <img src="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                                                    <div class="mail-contnet">
                                                                        <h5>Pavan kumar</h5>
                                                                        <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span>
                                                                    </div>
                                                                </a>
                                                                <a href="#">
                                                                    <div class="user-img"> <img src="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/images/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                                                    <div class="mail-contnet">
                                                                        <h5>Sonu Nigam</h5>
                                                                        <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span>
                                                                    </div>
                                                                </a>
                                                                <a href="#">
                                                                    <div class="user-img"> <img src="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/images/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                                                    <div class="mail-contnet">
                                                                        <h5>Arijit Sinh</h5>
                                                                        <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span>
                                                                    </div>
                                                                </a>
                                                                <a href="#">
                                                                    <div class="user-img"> <img src="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/images/users/genu.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                                                    <div class="mail-contnet">
                                                                        <h5>Genelia Deshmukh</h5>
                                                                        <span class="mail-desc">I love to do acting and dancing</span> <span class="time">9:08 AM</span>
                                                                    </div>
                                                                </a>
                                                                <a href="#" class="b-none">
                                                                    <div class="user-img"> <img src="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                                                    <div class="mail-contnet">
                                                                        <h5>Pavan kumar</h5>
                                                                        <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                        <div class="white-box">
                                                            <h3 class="box-title">Chat</h3>
                                                            <div class="chat-box">
                                                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><ul class="chat-list slimscroll" style="overflow: hidden; width: auto; height: 100%;" tabindex="5005">
                                                                    <li>
                                                                        <div class="chat-image"> <img alt="male" src="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/images/users/sonu.jpg"> </div>
                                                                        <div class="chat-body">
                                                                            <div class="chat-text">
                                                                                <h4>Sonu Nigam</h4>
                                                                                <p> Hi, All! </p>
                                                                                <b>10.00 am</b>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="odd">
                                                                        <div class="chat-image"> <img alt="Female" src="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/images/users/genu.jpg"> </div>
                                                                        <div class="chat-body">
                                                                            <div class="chat-text">
                                                                                <h4>Genelia</h4>
                                                                                <p> Hi, How are you Sonu? ur next concert? </p>
                                                                                <b>10.03 am</b>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="chat-image"> <img alt="male" src="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/images/users/ritesh.jpg"> </div>
                                                                        <div class="chat-body">
                                                                            <div class="chat-text">
                                                                                <h4>Ritesh</h4>
                                                                                <p> Hi, Sonu and Genelia, </p>
                                                                                <b>10.05 am</b>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul><div class="slimScrollBar" style="background: rgb(220, 220, 220) none repeat scroll 0% 0%; width: 0px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 319.49px;"></div><div class="slimScrollRail" style="width: 0px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group">
                                                                            <input class="form-control" placeholder="Say something" type="text"> <span class="input-group-btn">
                                                                    <button class="btn btn-success" type="button">Send</button>
                                                                    </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="white-box">
                                                            <h3 class="box-title">Order Status</h3>
                                                            <div class="table-responsive">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Invoice</th>
                                                                            <th>User</th>
                                                                            <th>Order date</th>
                                                                            <th>Amount</th>
                                                                            <th class="text-center">Status</th>
                                                                            <th class="text-center">Tracking Number</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><a href="javascript:void(0)" class="btn-link"> Order #53431</a></td>
                                                                            <td>Steve N. Horton</td>
                                                                            <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 22, 2014</span></td>
                                                                            <td>$45.00</td>
                                                                            <td class="text-center">
                                                                                <div class="label label-table label-success">Paid</div>
                                                                            </td>
                                                                            <td class="text-center">-</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a href="javascript:void(0)" class="btn-link"> Order #53432</a></td>
                                                                            <td>Charles S Boyle</td>
                                                                            <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 24, 2014</span></td>
                                                                            <td>$245.30</td>
                                                                            <td class="text-center">
                                                                                <div class="label label-table label-info">Shipped</div>
                                                                            </td>
                                                                            <td class="text-center"><i class="fa fa-plane"></i> CGX0089734531</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a href="javascript:void(0)" class="btn-link"> Order #53433</a></td>
                                                                            <td>Lucy Doe</td>
                                                                            <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 24, 2014</span></td>
                                                                            <td>$38.00</td>
                                                                            <td class="text-center">
                                                                                <div class="label label-table label-info">Shipped</div>
                                                                            </td>
                                                                            <td class="text-center"><i class="fa fa-plane"></i> CGX0089934571</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a href="javascript:void(0)" class="btn-link"> Order #53434</a></td>
                                                                            <td>Teresa L. Doe</td>
                                                                            <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 15, 2014</span></td>
                                                                            <td>$77.99</td>
                                                                            <td class="text-center">
                                                                                <div class="label label-table label-info">Shipped</div>
                                                                            </td>
                                                                            <td class="text-center"><i class="fa fa-plane"></i> CGX0089734574</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a href="javascript:void(0)" class="btn-link"> Order #53435</a></td>
                                                                            <td>Teresa L. Doe</td>
                                                                            <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 12, 2014</span></td>
                                                                            <td>$18.00</td>
                                                                            <td class="text-center">
                                                                                <div class="label label-table label-success">Paid</div>
                                                                            </td>
                                                                            <td class="text-center">-</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a href="javascript:void(0)" class="btn-link">Order #53437</a></td>
                                                                            <td>Charles S Boyle</td>
                                                                            <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 17, 2014</span></td>
                                                                            <td>$658.00</td>
                                                                            <td class="text-center">
                                                                                <div class="label label-table label-danger">Refunded</div>
                                                                            </td>
                                                                            <td class="text-center">-</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a href="javascript:void(0)" class="btn-link">Order #536584</a></td>
                                                                            <td>Scott S. Calabrese</td>
                                                                            <td><span class="text-muted"><i class="fa fa-clock-o"></i> Oct 19, 2014</span></td>
                                                                            <td>$45.58</td>
                                                                            <td class="text-center">
                                                                                <div class="label label-table label-warning">Unpaid</div>
                                                                            </td>
                                                                            <td class="text-center">-</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
												<?=d_submit(0)?>
											<?=form_close()?>
										</div>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

				</div>