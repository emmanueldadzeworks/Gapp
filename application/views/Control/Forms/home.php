
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$("input").change(function(){
    if (hour < 18) {
        greeting = "Good day";
    }
    $("#hide").click(function(){
        $("#text_hide").hide();
    });
    $("#show").click(function(){
        $("#text_hide").show();
    });
});
</script>


<div class="container-fluid">
    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Navigation</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
            <li><a href="index.html">Dashboard</a></li>
            <li><a href="#"><span>form</span></a></li>
            <li class="active"><span>Navigation</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->
    
    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Navigation</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                
                                
                                <!-- sample modal content -->
                                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h5 class="modal-title" id="myModalLabel"> New Navigation </h5>
                                            </div>
                                            <?=form_open_multipart("generic/forms/add_navigation" ); ?>
                                                
                                                <div class="modal-body">
                                                    <?=validation_errors()?>
                                                    <div class="form-group">
                                                        <label for="title" class="control-label mb-10">title:</label>
                                                        <input type="text" class="form-control" id="title" name="title">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="order" class="control-label mb-10">Order:</label>
                                                        <input type="number" class="form-control" id="order" name="order">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="message-text" class="control-label mb-10">Any content:</label>
                                                        <textarea class="form-control" id="body" name="body"></textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-danger" value="Save changes" />
                                                </div>
                                            <?=form_close()?>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <!-- Button trigger modal -->
                                <a data-toggle="modal" data-target="#myModal" class="btn btn-success mt-repeater-add"> + add</a> 
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Status</th>
                                            <th>Last Modified</th>
                                            <th>Forms</th>
                                            <th>operations</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Status</th>
                                            <th>Last Modified</th>
                                            <th>Forms</th>
                                            <th>operations</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($Navigation as $key => $value){ ?>
                                            <tr>
                                                <td><?=$value->title?></td>
                                                <td><?=$value->body?></td>
                                                <td><?=$value->status?></td>
                                                <td><?=$value->modified?></td>
                                                <td>
                                
                                                    <!-- sample modal content -->
                                                    <div id="<?=$value->slug?>" class="modal fade bs-example-modal-lg in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h5 class="modal-title" id="<?=$value->slug?>Label">Forms creater for <?=$value->title?> </h5>
                                                                </div>
                                                                <?php $class = array('class' => 'mt-repeater form-horizontal')?>
                                                                
                                                                    
                                                                    <div class="modal-body">
                                                                        <?=validation_errors()?>
                                                                        
                                                                        <div class="panel-wrapper collapse in">
                                                                            <div class="panel-body">
                                                                                <div class="form-wrap">
                                                                                <?php $Form_db = $this->view_forms_m->get_by(array('navigation_id' => $value->id)); ?>
                                                                                <?php $s_id = isset($Form_db[0]->id) ? "/".$Form_db[0]->id :"" ; ?>
                                                                                <?=form_open_multipart("generic/forms/add_forms/".$value->id."/".$value->slug."".$s_id , $class); ?>
                                                                                        <?php if($Form_db){ ?>
                                                                                            <?php foreach($Form_db as $f_value) { ?>
                                                                                                <?php $f_value  =  json_decode($f_value->array_form); ?>
                                                                                                <div data-repeater-list="<?=$value->slug?>" class="form-group mb-0 col-sm-12">
                                                                                                    <?php foreach($f_value as $ff_value) { ?>
                                                                                                        <?php foreach($ff_value as $s_value) { ?>
                                                                                                            <div data-repeater-item class="row">
                                                                                                                <!-- jQuery Repeater Container Starts -->
                                                                                                                <div class="col-sm-2">
                                                                                                                    <label class="control-label mb-10"> Lable</label>
                                                                                                                    <input type="text" name="lable" value="<?=humanize($s_value->lable)?>" class="form-control" placeholder="Pls enter Field Name">
                                                                                                                </div>
                                                                                                                <div class="col-sm-2">
                                                                                                                    <label class="control-label mb-10">Input Type</label>
                                                                                                                    <select name="input_type" class="form-control" >
                                                                                                                        <option value="number" <?=$s_value->input_type === "number" ? "selected" : ""?>> Number </option>
                                                                                                                        <option value="select" <?=$s_value->input_type === "select" ? "selected" : ""?>> Select </option>
                                                                                                                        <option value="select_query" <?=$s_value->input_type === "select_query" ? "selected" : ""?>> Select Query</option>
                                                                                                                        <option value="text" <?=$s_value->input_type === "text" ? "selected" : ""?>> Text </option>
                                                                                                                        <option value="textarea" <?=$s_value->input_type === "textarea" ? "selected" : ""?>> Textarea</option>
                                                                                                                    </select>
                                                                                                                </div>
                                                                                                                <div class="col-sm-2">
                                                                                                                    <label class="control-label mb-10">If select </label>
                                                                                                                    <textarea name="select_array" class="form-control"><?=$s_value->select_array?></textarea>
                                                                                                                    <span class="text-muted">e.g "Mr. | Mrs. | Dr. | Proff "</span>
                                                                                                                </div>
                                                                                                                <div class="col-sm-2">
                                                                                                                    <label class="control-label mb-10">If select Query </label>
                                                                                                                    <select name="select_query" class="form-control" >
                                                                                                                        <option value=""> Select the proper col</option>
                                                                                                                        <?php 
                                                                                                                        
                                                                                                                            $CI = get_instance();
                                                                                                                            $CI->load->model('navigation_m');
                                                                                                                            $CI->load->model('view_forms_m');
                                                                                                                            $CI->load->model('form_data_m');

                                                                                                                            
                                                                                                                            foreach($CI->navigation_m->get() as $ff_value){
                                                                                                                                ?><optgroup label="<?=$ff_value->title?>"><?php
                                                                                                                                foreach($CI->view_forms_m->get_by(array("navigation_id"=> $ff_value->id )) as $ss_value){
                                                                                                                                    foreach(json_decode($ss_value->array_form) as $tt_value){
                                                                                                                                        foreach( $tt_value as $ffo_key => $ffo_value){
                                                                                                                                            $val = trim('('.$ff_value->slug.')('.$ffo_value->lable.')') ;
                                                                                                                                            $db_val = trim($s_value->select_query) ;
                                                                                                                                            ?><option value="<?=$val?>" <?=$db_val == $val ? "selected" : ""?>> <?=humanize($ffo_value->lable)?> </option><?php
                                                                                                                                        }
                                                                                                                                        break;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                ?></optgroup><?php
                                                                                                                            }
                                                                                                                        ?>
                                                                                                                    </select>
                                                                                                                    <span class="text-muted"></span>
                                                                                                                </div>
                                                                                                                <div class="col-sm-2">
                                                                                                                    <label class="control-label mb-10">Number Range</label>
                                                                                                                    <input type="text" class="form-control " data-mask="99-99" name="number_range" value="<?=isset($s_value->number_range) ? $s_value->number_range : "";?>">
                                                                                                                    <span class="text-muted">e.g "0-90"</span>
                                                                                                                </div>
                                                                                                                <div class="col-sm-2">
                                                                                                                    <label class="control-label mb-10">Place Holder</label>
                                                                                                                    <input type="text" class="form-control" name="placeholder" value="<?=!isset($s_value->placeholder) ? '' :$s_value->placeholder ?>">
                                                                                                                </div>
                                                                                                                <div class="col-sm-2">
                                                                                                                    <div class="checkbox checkbox-primary">
                                                                                                                        <input id="<?=$value->slug?>" type="checkbox" checked="">
                                                                                                                        <label for="<?=$value->slug?>"> Required </label>
                                                                                                                    </div>
                                                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                                                                        <i class="fa fa-close"></i> Delete</a>
                                                                                                                </div>
                                                                                                                <!-- jQuery Repeater Container Ends -->
                                                                                                            </div>
                                                                                                        <?php } break; ?>
                                                                                                    <?php } ?>
                                                                                                </div>
                                                                                            <?php } ?>
                                                                                        <?php } else { ?>
                                                                                            <div data-repeater-list="<?=$value->slug?>" class="form-group mb-0 col-sm-12">
                                                                                                <div data-repeater-item class="row">
                                                                                                    <!-- jQuery Repeater Container Starts -->
                                                                                                    <div class="col-sm-2">
                                                                                                        <label class="control-label mb-10"> Lable</label>
                                                                                                        <input type="text" name="lable" class="form-control" placeholder="Pls enter Field Name">
                                                                                                    </div>
                                                                                                    <div class="col-sm-2">
                                                                                                        <label class="control-label mb-10">Input Type</label>
                                                                                                        <select name="input_type" class="form-control" >
                                                                                                            <option value="number"> Number </option>
                                                                                                            <option value="select"> Select </option>
                                                                                                            <option value="select_query"> Select Query </option>
                                                                                                            <option value="text"> Text</option>
                                                                                                            <option value="textarea"> Textarea</option>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                    <div class="col-sm-2">
                                                                                                        <label class="control-label mb-10">If select </label>
                                                                                                        <textarea name="select_array" class="form-control"></textarea>
                                                                                                        <span class="text-muted">e.g "Mr. | Mrs. | Dr. | Proff "</span>
                                                                                                    </div>
                                                                                                                <div class="col-sm-2">
                                                                                                                    <label class="control-label mb-10">If select Query </label>
                                                                                                                    <select name="select_query" class="form-control" >
                                                                                                                        <option value=""> Select the proper col</option>
                                                                                                                        <?php 
                                                                                                                        
                                                                                                                            $CI = get_instance();
                                                                                                                            $CI->load->model('navigation_m');
                                                                                                                            $CI->load->model('view_forms_m');
                                                                                                                            $CI->load->model('form_data_m');

                                                                                                                            
                                                                                                                            foreach($CI->navigation_m->get() as $ff_value){
                                                                                                                                ?><optgroup label="<?=$ff_value->title?>"><?php
                                                                                                                                foreach($CI->view_forms_m->get_by(array("navigation_id"=> $ff_value->id )) as $ss_value){
                                                                                                                                    foreach(json_decode($ss_value->array_form) as $tt_value){
                                                                                                                                        foreach( $tt_value as $ffo_key => $ffo_value){
                                                                                                                                            $val = trim('('.$ff_value->slug.')('.$ffo_value->lable.')') ;
                                                                                                                                            ?><option value="<?=$val?>" > <?=humanize($ffo_value->lable)?> </option><?php
                                                                                                                                        }
                                                                                                                                        break;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                ?></optgroup><?php
                                                                                                                            }
                                                                                                                        ?>
                                                                                                                    </select>
                                                                                                                    <span class="text-muted"></span>
                                                                                                                </div>
                                                                                                    <div class="col-sm-2">
                                                                                                        <label class="control-label mb-10">Number Range</label>
                                                                                                        <input type="text" class="form-control " data-mask="99-99">
                                                                                                        <span class="text-muted">e.g "0-90"</span>
                                                                                                    </div>
                                                                                                    <div class="col-sm-2">
                                                                                                        <label class="control-label mb-10">Place Holder</label>
                                                                                                        <input type="text" class="form-control" name="placeholder">
                                                                                                    </div>
                                                                                                    <!-- jQuery Repeater Container Ends -->
                                                                                                    <div class="col-sm-2">

                                                                                                        <div class="checkbox checkbox-primary">
                                                                                                            <input id="<?=$value->slug?>" type="checkbox" checked="">
                                                                                                            <label for="<?=$value->slug?>"> Required </label>
                                                                                                        </div>
                                                                                                        <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                                                            <i class="fa fa-close"></i> Delete</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                        <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                                                                            <i class="fa fa-plus"></i> Add
                                                                                        </a>

                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                            <input type="submit" class="btn btn-danger" value="Save changes" />
                                                                                        </div>
                                                                                    <?=form_close()?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                                    <!-- Button trigger modal -->
                                                    <a data-toggle="modal" data-target="#<?=$value->slug?>" class="btn btn-success mt-repeater-add"> +/-</a> 
                                                </td>
                                                <td>
                                                    
                                                    <!-- sample modal content -->
                                                    <div id="nav_<?=$value->slug?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h5 class="modal-title" id="myModalLabel"> Edit <?=$value->title?> </h5>
                                                                </div>
                                                                <?=form_open_multipart("generic/forms/add_navigation/".$value->id ); ?>
                                                                    
                                                                    <div class="modal-body">
                                                                        <?=validation_errors()?>
                                                                        <div class="form-group">
                                                                            <label for="title" class="control-label mb-10">title:</label>
                                                                            <input type="text" class="form-control" id="title" name="title" value="<?=$value->title?>">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="order" class="control-label mb-10">Order:</label>
                                                                            <input type="number" class="form-control" id="order" name="order" value="<?=$value->order?>">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="message-text" class="control-label mb-10">Any content:</label>
                                                                            <textarea class="form-control" id="body" name="body"><?=$value->body?></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <input type="submit" class="btn btn-danger" value="Save changes" />
                                                                    </div>
                                                                <?=form_close()?>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                                    <?=btn_edit_model("nav_".$value->slug)?>
                                                    <!-- sample modal content -->
                                                    <div id="btn_<?=$value->slug?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h5 class="modal-title" id="myModalLabel"> Edit <?=$value->title?> </h5>
                                                                </div>
                                                                <?=form_open_multipart("generic/forms/add_navigation/".$value->id ); ?>
                                                                    
                                                                    <div class="modal-body">
                                                                        <?=validation_errors()?>
                                                                        <div class="form-group">
                                                                            <label for="title" class="control-label mb-10">title:</label>
                                                                            <input type="text" class="form-control" id="title" name="title" value="<?=$value->title?>">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="order" class="control-label mb-10">Order:</label>
                                                                            <input type="number" class="form-control" id="order" name="order" value="<?=$value->order?>">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="message-text" class="control-label mb-10">Any content:</label>
                                                                            <textarea class="form-control" id="body" name="body"><?=$value->body?></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <input type="submit" class="btn btn-danger" value="Save changes" />
                                                                    </div>
                                                                <?=form_close()?>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                                    <?=btn_add_model("btn_".$value->slug)?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
    
    
</div>
