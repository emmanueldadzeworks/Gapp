<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Jetson I Fast build Admin dashboard for any platform</title>
	<meta name="description" content="Jetson is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Jetson Admin, Jetsonadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	
    <!-- Data table CSS -->
    <?php echo link_tag('resource/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css'); ?>
	<?php echo link_tag('resource/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css'); ?>
		
    <!-- Custom CSS -->
	<?php echo link_tag('resource/dist/css/style.css'); ?>
	

	<!-- Bootstrap Colorpicker CSS -->
	<?php echo link_tag('resource/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css'); ?>
	
	<!-- select2 CSS -->
	<?php echo link_tag('resource/vendors/bower_components/select2/dist/css/select2.min.css'); ?>
		
	<!-- Bootstrap Dropify CSS -->
	<?php echo link_tag('resource//vendors/bower_components/dropify/dist/css/dropify.min.css'); ?>
	
		
	<!-- switchery CSS -->
	<?php echo link_tag('resource/vendors/bower_components/switchery/dist/switchery.min.css'); ?>
		
	<!-- bootstrap-select CSS -->
	<?php echo link_tag('resource/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css'); ?>
		
	<!-- bootstrap-tagsinput CSS -->
	<?php echo link_tag('resource/vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'); ?>
		
	<!-- bootstrap-touchspin CSS -->
	<?php echo link_tag('resource/vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'); ?>
		
	<!-- multi-select CSS -->
	<?php echo link_tag('resource/vendors/bower_components/multiselect/css/multi-select.css'); ?>
		
	<!-- Bootstrap Switches CSS -->
	<?php echo link_tag('resource/vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css'); ?>
		
	<!-- Bootstrap Datetimepicker CSS -->
	<?php echo link_tag('resource/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'); ?>
		 
		
</head>

<body style="zoom: 0.8;">
	<!-- Preloader -->
	<div class="preloader-it">
		<div class="la-anim-1"> loading ...</div>
	</div>
    <!-- /Preloader -->
    
    