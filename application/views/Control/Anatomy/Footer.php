
        <!-- Footer -->
        <footer class="footer container-fluid pl-30 pr-30">
            <div class="row">
                <div class="col-sm-12">
                    <p>2017 &copy; Generic Applicatin by Emmanuel Dadzie</p>
                </div>
            </div>
        </footer>
        <!-- /Footer -->
        
    </div>
    <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
    
    


    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	
    <!-- Data table JavaScript -->
	<script src="<?php echo base_url(); ?>resource/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>resource/dist/js/dataTables-data.js"></script>

	<!-- Slimscroll JavaScript -->
	<script src="<?php echo base_url(); ?>resource/dist/js/jquery.slimscroll.js"></script>
	
	<!-- Progressbar Animation JavaScript -->
	<script src="<?php echo base_url(); ?>resource/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url(); ?>resource/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="<?php echo base_url(); ?>resource/dist/js/dropdown-bootstrap-extended.js"></script>
	
	<!-- Sparkline JavaScript -->
	<script src="<?php echo base_url(); ?>resource/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="<?php echo base_url(); ?>resource/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="<?php echo base_url(); ?>resource/vendors/bower_components/switchery/dist/switchery.min.js"></script>
	
	<!-- EChartJS JavaScript -->
	<script src="<?php echo base_url(); ?>resource/vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
	<script src="<?php echo base_url(); ?>resource/vendors/echarts-liquidfill.min.js"></script>
	
    <script src="<?php echo base_url(); ?>resource/dist/js/dashboard-data.js"></script>
    
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url(); ?>resource/global/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>resource/global/js/jquery.repeater.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>resource/global/js/form-repeater.min.js" type="text/javascript"></script>
    

    <!-- Moment JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>resource/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
    
    <!-- Bootstrap Colorpicker JavaScript -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    
    <!-- Select2 JavaScript -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
    
    <!-- Bootstrap Select JavaScript -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    
    <!-- Bootstrap Tagsinput JavaScript -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    
    <!-- Bootstrap Touchspin JavaScript -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
    
    <!-- Multiselect JavaScript -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/multiselect/js/jquery.multi-select.js"></script>
        
    <!-- Bootstrap Switch JavaScript -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    
    <!-- Bootstrap Datetimepicker JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>resource/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    
    <!-- Form Advance Init JavaScript -->
    <script src="<?php echo base_url(); ?>resource/dist/js/form-advance-data.js"></script>
		
	<script src="<?php echo base_url(); ?>resource/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
        
    
	<script src="<?php echo base_url(); ?>resource/dist/js/modal-data.js"></script>    
    <!-- jQuery file upload -->
		
    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="<?php echo base_url(); ?>resource/vendors/bower_components/dropify/dist/js/dropify.min.js"></script>
    
    <!-- Form Flie Upload Data JavaScript -->
    <script src="<?php echo base_url(); ?>resource/dist/js/form-file-upload-data.js"></script>
	<!-- Toast JavaScript -->
	<script src="<?php echo base_url(); ?>resource/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <?php 
        if (isset($notify)) {
            if (is_array($notify)) {
               foreach ($notify as $key => $value) {
                   echo notification($value);
               }
            }
        }
    ?>
	<!-- Init JavaScript -->
	<script src="<?php echo base_url(); ?>resource/dist/js/init.js"></script>
</body>
    </html>

