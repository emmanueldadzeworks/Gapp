<div class="wrapper">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                        </div>
                        <h4 class="page-title"> Notifications</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30"></h4>
                            <ul class="list-group m-b-0 user-list">
                            </ul>
                            <?php $count = 0; ?>
                            <?php foreach ($Nofification as $key => $value) { ?>
                                <?php $count++; ?>
                                <?php $data = $this->request_m->get($value->process_id);?>
                                <li class="list-group-item">
                                    <a href="#!" class="user-list-item">
                                        <div class="user-desc">
                                            <span class="name"><?=status_presenter($data->status)?></span><br/>
                                            <span class="desc">Comment<br/><b style="font-size:22px"><?=nl2br($value->comment)?></b></span><br/><br/>
                                            <span class="time"><?= humanize($data->request_type)?>|<?=$value->created?></span><br/>
                                            <span class="time"><b><?=$data->code?></b></span>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>

                            <?php if($count == 0) { ?>

                                <li class="list-group-item">
                                    <a href="#!" class="user-list-item">
                                        <div class="user-desc">
                                            <span class="name">Empty Nofification</span>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->
