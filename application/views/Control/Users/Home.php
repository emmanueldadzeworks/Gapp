
<div class="container-fluid">
    
    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">form element</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
            <li><a href="index.html">Dashboard</a></li>
            <li><a href="#"><span>form</span></a></li>
            <li class="active"><span>form-element</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->
    
    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark"> Users </h6>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                
                                
                                <!-- sample modal content -->
                                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h5 class="modal-title" id="myModalLabel"> New Navigation </h5>
                                            </div>
                                            <?=form_open_multipart("generic/forms/add_navigation" ); ?>
                                                
                                                <div class="modal-body">
                                                    <?=validation_errors()?>
                                                    <div class="form-group">
                                                        <label for="title" class="control-label mb-10">title:</label>
                                                        <input type="text" class="form-control" id="title" name="title">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="order" class="control-label mb-10">Order:</label>
                                                        <input type="number" class="form-control" id="order" name="order">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="message-text" class="control-label mb-10">Any content:</label>
                                                        <textarea class="form-control" id="body" name="body"></textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-danger" value="Save changes" />
                                                </div>
                                            <?=form_close()?>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <!-- Button trigger modal -->
                                <a data-toggle="modal" data-target="#myModal" class="btn btn-success mt-repeater-add"> + add</a> 
                                <table id="datable_1" class="table table-hover display  pb-30" >
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Status</th>
                                            <th>Last Modified</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Status</th>
                                            <th>Last Modified</th>
                                            <th>operations</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($users as $key => $value){ ?>
                                            <tr>
                                                <td><?=$value->first_name?></td>
                                                <td><?=$value->last_name?></td>
                                                <td><?=$value->email?></td>
                                                <td><?=$value->modified?></td>
                                                <td><?=un_btn_activate($value->status , 'generic/users/status/'.$value->id ,"")?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
    
    
</div>
