

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured Employee Self Service.">
        <meta name="author" content="Emmanuel Kwabena Dadzie (020 913 7654)">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://zentechgh.bypronto.com/wp-content/uploads/sites/2162/2017/02/cropped-favicon.png">

        <!-- App title -->
        <title><?php echo config_item('site_name'); ?></title>

        <!-- App CSS -->
        <?php echo link_tag('resource/assets/css/bootstrap.min.css'); ?>
        <?php echo link_tag('resource/assets/css/core.css'); ?>
        <?php echo link_tag('resource/assets/css/components.css'); ?>
        <?php echo link_tag('resource/assets/css/icons.css'); ?>
        <?php echo link_tag('resource/assets/css/pages.css'); ?>
        <?php echo link_tag('resource/assets/css/menu.css'); ?>
        <?php echo link_tag('resource/assets/css/responsive.css'); ?>
        
        <?php echo link_tag('resource/assets/plugins/toastr/toastr.min.css'); ?>"

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url(); ?>resource/assets/js/modernizr.min.js"></script>
        
                <?php
                    $bg_picture = $this->go_main->curl_uri."/app/uploads/ess_background.png";
                    $file_headers = @get_headers($bg_picture);
                    if ($file_headers[0] !== 'HTTP/1.0 404 Not Found') {
                        //$bg_picture = $this->go_main->curl_uri."/app/uploads/ess_background.png";
                        ?>
                        <style>
                            .account-pages {
                                background: url(<?=$bg_picture?>) !important;
                                background-size: cover !important;
        
                            }
                        </style>
                    <?php } ?>
                
            </head>
    <body style="zoom: 0.8;">

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">
                <a href="<?php echo base_url(); ?>" class="logo">
                    <img src="<?php echo base_url(); ?>resource/assets/images/hrmlogo.png" style="width: 40%;" >
                </a>
                <h5 class="text-muted m-t-0 font-600"><?php echo config_item('meta_title'); ?></h5>
            </div>

            <div class="m-t-40 card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Welcome Back</h4>
                </div>
                <div class="panel-body">
                    <?php $sign_up_guid = array('class' => 'text-center' ); ?>
                    <?php echo form_open(null , $sign_up_guid ); ?>
						<div class="user-thumb">
							<img src="<?php echo $user_picture; ?>" class="img-responsive img-circle img-thumbnail" alt="thumbnail">
                            <h4><?php echo  _l($user->employee); ?></h4>
						</div>
                        <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
                        <?php if($this->session->flashdata('message-danger')){ ?>
                          <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('message-danger'); ?>
                          </div>
                        <?php } ?>
                        <?php if($this->session->flashdata('message-success')){ ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('message-success'); ?>
                            </div>
                        <?php } ?>
                        <?php
                          if (!empty(get_cookie("lock_account", TRUE)) && $this->uri->segment(3) !== "account") {
                            $return_cookies = json_decode(get_cookie("lock_account", TRUE));
                            $data = array(
                              'email'  => $return_cookies->email,
                            );

                            echo form_hidden($data);
                            echo '<div class="form-group"> <p class="text-muted m-t-10"> Enter your password to access the admin. </p>  <div class="input-group m-t-30"> <input type="password" class="form-control" placeholder="Password" required="" autocomplete="off" name="password" > <span class="input-group-btn"> <button type="submit" class="btn w-sm waves-effect waves-light"> Log In </button> </span> </div> </div>';

                          } else {
                            echo '<div class="form-actions"> <a href="'.site_url('welcome/lock').'" class="btn red uppercase">'._l("Proceed login").'</a> </div>';
                          }
                        ?> 

                        <p class="row">
                            <center class="col-xs-12">
                                Powered by Zentech.
                            </center>
                        </p>
                    <?php echo form_close(); ?>


                </div>
            </div>
            <!-- end card-box-->

            <div class="row">
                <div class="col-sm-12 text-center">
                    <p class="text-muted"><a href="<?php echo site_url("welcome/logout"); ?>">Not <?php echo  _l($user->employee); ?> ?</a></p>
                </div>
            </div>
            
        </div>
        <!-- end wrapper page -->
        

        
    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/detect.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/fastclick.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/waves.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.app.js"></script>
	
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url(); ?>resource/assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
        <?php 
            if (isset($notify)) {
                if (is_array($notify)) {
                   foreach ($notify as $key => $value) {
                       echo notification($value);
                   }
                }
            }
        ?>

	</body>

</html>