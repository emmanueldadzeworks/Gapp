<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured Employee Self Service.">
        <meta name="author" content="Emmanuel Kwabena Dadzie (020 913 7654)">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://zentechgh.bypronto.com/wp-content/uploads/sites/2162/2017/02/cropped-favicon.png">

        <!-- App title -->
        <title><?php echo config_item('site_name'); ?></title>

        <!-- App CSS -->
        <?php echo link_tag('resource/assets/css/bootstrap.min.css'); ?>
        <?php echo link_tag('resource/assets/css/core.css'); ?>
        <?php echo link_tag('resource/assets/css/components.css'); ?>
        <?php echo link_tag('resource/assets/css/icons.css'); ?>
        <?php echo link_tag('resource/assets/css/pages.css'); ?>
        <?php echo link_tag('resource/assets/css/menu.css'); ?>
        <?php echo link_tag('resource/assets/css/responsive.css'); ?>
        
        <?php echo link_tag('resource/assets/plugins/toastr/toastr.min.css'); ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url(); ?>resource/assets/js/modernizr.min.js"></script>
        <?php
            $bg_picture = $this->go_main->curl_uri."/app/uploads/ess_background.jpeg";
            $file_headers = @get_headers($bg_picture);

        ?>
        <style>
            .account-pages {
                background: url(<?=$bg_picture?>)  !important;
                background-size: 100% 100%, cover !important;
            }
        </style>
    </head>
    <body style="zoom: 0.8;">

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">
                <a href="<?php echo base_url(); ?>" class="logo">
                    <!--img src="app/uploads/logo.jpeg" style="width: 40%;" 
                    <img src="resource/assets/images/hrmlogo.png" style="width: 40%;" > -->
                </a>
                <h5 class="text-muted m-t-0 font-600"><?php echo config_item('meta_title'); ?></h5>
            </div>
        	<div class="m-t-40 card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
                </div>
                <div class="panel-body">
                    <?php echo form_open(null , array("class" => "form-horizontal m-t-20") ); ?>
                    
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required="" placeholder="Username" name="username" value="hrms@gcb.com.gh"> <!--Essadmin@zeness.com and afrempong-manso-->
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" placeholder="Password" name="password" value="welcome55">
                                <!-- zentechGH1234 -->
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <div class="checkbox checkbox-custom">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup">
                                        Remember me
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-sm-12">
                                <a href="<?php /*echo site_url('welcome/reset');*/ ?>#!" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password? Contact your Server manager</a>

                                    <p class="row">
                                        <center class="col-xs-12">
                                            Powered by Zentech.
                                        </center>
                                    </p>
                            </div>
                        </div>
                    <?php echo form_close(); ?>

                </div>
            </div>
            <!-- end card-box-->
            
        </div>
        <!-- end wrapper page -->
        

        
    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/detect.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/fastclick.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/waves.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url(); ?>resource/assets/js/jquery.app.js"></script>
	
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url(); ?>resource/assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
        <?php 
            if (isset($notify)) {
                if (is_array($notify)) {
                   foreach ($notify as $key => $value) {
                       echo notification($value);
                   }
                }
            }
        ?>

	</body>

</html>