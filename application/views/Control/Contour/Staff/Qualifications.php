<style>
    fieldset{
        background-color:#cdd9e6;
        padding:20px 0px 20px 0px ;
    }
</style>
<div class="wrapper">
<div class="container">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">  </div>
            <h4 class="page-title"> Qualifications </h4>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"> Qualifications </h4>
                <?php $class = array('id' => 'fileupload' ,'target' => '_blank' ); ?>
                <?php echo form_open_multipart(null , $class); ?>
                    <?=validation_errors()?>
                        <div class="repeater-custom-show-hide">
                            <div data-repeater-list="qualifications">
                            
                                <?php foreach($employees_qualifications as $qulification){ ?>
                                    <div data-repeater-item class="row">
                                        <fieldset>
                                            <div class="form-group col-lg-12">
                                                <div class="dropify-wrapper has-preview" style="height: 314px;">
                                                    <div class="dropify-message">
                                                        <span class="file-icon"></span> 
                                                        <p>Drag and drop a file here or click</p>
                                                        <p class="dropify-error">Ooops, something wrong .</p>
                                                    </div>
                                                    <div class="dropify-loader"></div>
                                                    <div class="dropify-errors-container">
                                                        <ul></ul>
                                                    </div>
                                                    <input type="file" name="certificate" class="dropify" data-height="300" data-max-file-size="5M" accept="image/*" data-default-file="<?=$this->go_main->curl_uri."/".$qulification->picture?>">
                                                    <button type="button" class="dropify-clear"  >Remove</button>
                                                    <div class="dropify-preview">
                                                        <span class="dropify-render"></span>
                                                        <div class="dropify-infos">
                                                            <div class="dropify-infos-inner">
                                                                <p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>
                                                                <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3">
                                                    <label class="col-lg-12 control-label">Area Of Specialisation</label>
                                                    <select class="form-control" name="area_of_specialisation_id" id="area_of_specialisation_id" required>
                                                        <?php foreach ( $area_of_specialisations as $area_of_specialisation){ ?>
                                                            <option value="<?=$area_of_specialisation->area_of_specialisation_id?>" <?=$qulification->area_of_specialisation_id == $area_of_specialisation->area_of_specialisation_id ? "selected" : ""?> > <?=$area_of_specialisation->description?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="col-lg-3">
                                                    <label class="col-lg-12 control-label">Institution*</label>
                                                    <input class="form-control" name="institution" type="text" value="<?=$qulification->institution?>" required/>
                                                </div>

                                                <div class="col-lg-2">
                                                    <label class="col-lg-12 control-label">Qualification *</label>
                                                    <input class="form-control" name="qualification" type="text" value="<?=$qulification->qualification?>" required/>
                                                </div>

                                                <div class="col-lg-1">
                                                    <label class="col-lg-12 control-label">Start Date*</label>
                                                    <input class="form-control" name="start_date" type="text" value="<?=$qulification->start_date?>" required/>
                                                </div>

                                                <div class="col-lg-1">
                                                    <label class="col-lg-12 control-label">End Date *</label>
                                                    <input class="form-control" name="end_date" type="text" value="<?=$qulification->end_date?>" required/>
                                                </div>
                                
                                                <div class="col-sm-1">
                                                    <span class="col-lg-12" > - </span>
                                                    <span data-repeater-delete class="btn btn-danger btn-lg">
                                                        <span class="glyphicon glyphicon-remove"></span> Delete
                                                    </span>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                <?php } ?>
                                
                                <?php if(count($employees_qualifications) == 0){ ?>
                                    <div data-repeater-item class="row">
                                        <fieldset>
                                            <div class="form-group col-lg-12">
                                                <div class="dropify-wrapper has-preview" style="height: 314px;">
                                                    <div class="dropify-message">
                                                        <span class="file-icon"></span> 
                                                        <p>Drag and drop a file here or click</p>
                                                        <p class="dropify-error">Ooops, something wrong .</p>
                                                    </div>
                                                    <div class="dropify-loader"></div>
                                                    <div class="dropify-errors-container">
                                                        <ul></ul>
                                                    </div>
                                                    <input type="file" name="certificate" class="dropify" data-height="300" data-max-file-size="5M" accept="image/*" ><button type="button" class="dropify-clear">Remove</button>
                                                    <div class="dropify-preview">
                                                        <span class="dropify-render"></span>
                                                        <div class="dropify-infos">
                                                            <div class="dropify-infos-inner">
                                                                <p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>
                                                                <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3">
                                                    <label class="col-lg-12 control-label">Area Of Specialisation</label>
                                                    <select class="form-control" name="area_of_specialisation_id" id="area_of_specialisation_id" required>
                                                        <option value="" >Select any Area Of Specialisation</option>
                                                        <?php foreach ( $area_of_specialisations as $area_of_specialisation){ ?>
                                                            <option value="<?=$area_of_specialisation->area_of_specialisation_id?>"><?=$area_of_specialisation->description?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="col-lg-3">
                                                    <label class="col-lg-12 control-label">Institution*</label>
                                                    <input class="form-control" name="institution" type="text" required/>
                                                </div>

                                                <div class="col-lg-2">
                                                    <label class="col-lg-12 control-label">Qualification *</label>
                                                    <input class="form-control" name="qualification" type="text" required/>
                                                </div>

                                                <div class="col-lg-1">
                                                    <label class="col-lg-12 control-label">Start Date*</label>
                                                    <input class="form-control" name="start_date" type="text" required/>
                                                </div>

                                                <div class="col-lg-1">
                                                    <label class="col-lg-12 control-label">End Date *</label>
                                                    <input class="form-control" name="end_date" type="text" required/>
                                                </div>
                                
                                                <div class="col-sm-1">
                                                    <span class="col-lg-12" > - </span>
                                                    <span data-repeater-delete class="btn btn-danger btn-lg">
                                                        <span class="glyphicon glyphicon-remove"></span> Delete
                                                    </span>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-9" >  </div>
                                <div class="col-lg-1" >  </div>
                                <div class="col-lg-3">
                                    <span data-repeater-create class="btn btn-info btn-md">
                                        <span class="glyphicon glyphicon-plus"></span> Add
                                    </span>
                                </div>
                            </div>

                    
                            <hr/>
                    
                        </div>
                    </fieldset>

                    <?php d_submit($pending); ?>
                <?php echo form_close(); ?>

            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
