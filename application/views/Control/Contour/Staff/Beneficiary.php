<div class="wrapper">
<div class="container">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">  </div>
            <h4 class="page-title"> Beneficiary </h4>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-8">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"> Beneficiary </h4>
                
                <?php $class = array('id' => 'fileupload'); ?>
                <?php echo form_open_multipart(null , $class); ?>

                    <?php $multiple_repeaters = true; ?>
                    <fieldset>
                        <div class="repeater-custom-show-hide">
                            <div data-repeater-list="beneficiary">
                                <?php foreach($employee_beneficiaries as $beneficiarie){ ?>
                                    <div data-repeater-item class="row">
                                        <div class="form-group col-lg-12">
                                            <div class="col-lg-3">
                                                <label class="col-lg-12 control-label">Relationship</label>
                                                <select class="form-control" name="relationship_id" id="relationship" required>
                                                    <?php foreach ( $relationships as $relationship){ ?>
                                                        <option value="<?=$relationship->relationship_id?>" <?=$beneficiarie->relationship_id == $relationship->relationship_id ? "selected" : ""?> ><?=$relationship->description?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="col-lg-12 control-label">Full Name*</label>
                                                <input class="form-control" name="full_name" type="text" value="<?=$beneficiarie->full_name?>" required/>
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="col-lg-12 control-label">Entitlement Percentage(%) *</label>
                                                <input class="form-control" name="entitlement" type="text" pattern="\d+" value="<?=$beneficiarie->entitlement?>" required/>
                                            </div>
                            
                                            <div class="col-sm-1">
                                                <span class="col-lg-12" > - </span>
                                                <span data-repeater-delete class="btn btn-danger btn-lg">
                                                    <span class="glyphicon glyphicon-remove"></span> Delete
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if($this->input->post()){ ?>
                                    <?php foreach($this->input->post()['beneficiary'] as $beneficiarie){ ?>
                                        <div data-repeater-item class="row">
                                            <div class="form-group col-lg-12">
                                                <div class="col-lg-3">
                                                    <label class="col-lg-12 control-label">Relationship</label>
                                                    <select class="form-control" name="relationship_id" id="relationship" required>
                                                        <?php foreach ( $relationships as $relationship){ ?>
                                                            <option value="<?=$relationship->relationship_id?>" <?=$beneficiarie['relationship_id'] == $relationship->relationship_id ? "selected" : ""?> ><?=$relationship->description?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="col-lg-3">
                                                    <label class="col-lg-12 control-label">Full Name*</label>
                                                    <input class="form-control" name="full_name" type="text" value="<?=$beneficiarie['full_name']?>" required/>
                                                </div>

                                                <div class="col-lg-3">
                                                    <label class="col-lg-12 control-label">Entitlement Percentage(%) *</label>
                                                    <input class="form-control" name="entitlement" type="text" pattern="\d+" value="<?=$beneficiarie['entitlement']?>" required/>
                                                </div>
                                
                                                <div class="col-sm-1">
                                                    <span class="col-lg-12" > - </span>
                                                    <span data-repeater-delete class="btn btn-danger btn-lg">
                                                        <span class="glyphicon glyphicon-remove"></span> Delete
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                
                                <?php if(count($employee_beneficiaries) == 0){ ?>
                                    <div data-repeater-item class="row">
                                        <div class="form-group col-lg-12">
                                            <div class="col-lg-3">
                                                <label class="col-lg-12 control-label">Relationship</label>
                                                <select class="form-control" name="relationship_id" id="relationship_id" required>
                                                    <option value="" >Please select your relation</option>
                                                    <?php foreach ( $relationships as $relationship){ ?>
                                                        <option value="<?=$relationship->relationship_id?>" ><?=$relationship->description?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="col-lg-12 control-label">Full Name*</label>
                                                <input class="form-control" name="full_name" type="text" required/>
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="col-lg-12 control-label">Entitlement Percentage(%) *</label>
                                                <input class="form-control" name="entitlement" type="text" pattern="\d+" required/>
                                            </div>
                            
                                            <div class="col-sm-1">
                                                <span class="col-lg-12" > - </span>
                                                <span data-repeater-delete class="btn btn-danger btn-lg">
                                                    <span class="glyphicon glyphicon-remove"></span> Delete
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-9" >  </div>
                                <div class="col-lg-3">
                                    <span data-repeater-create class="btn btn-info btn-md">
                                        <span class="glyphicon glyphicon-plus"></span> Add
                                    </span>
                                </div>
                            </div>

                    
                            <hr/>
                    
                        </div>
                    </fieldset>
                    
                    <?php d_submit($pending); ?>
                <?php echo form_close(); ?>

            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
