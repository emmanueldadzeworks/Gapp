<?php
class Migration_Set_Super_root_user extends CI_Migration {

	public function up()
	{
		$data = array(
		        'email' 					=> 'emmanueldadzeworks@gmail.com',
		        'first_name' 				=> 'Emmanuel',
		        'last_name' 				=> 'Dadzie',
		        'picture' 					=> 'resource/img/avatar1.png',
		        'status' 					=> 'Active',
		        'password' 					=> 	md5('gappgforms'),
		);

		$this->db->insert( 'users', $data);
	}

	public function down()
	{
		$this->db->empty_table( 'users' );
	}
}