<?php
class Migration_Create_view_forms extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'navigation_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'table_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'array_form' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'array_view' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'default' => 'Active'
			),
			'buttons' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'nb' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('view_forms');
	}

	public function down()
	{
		$this->dbforge->drop_table('view_forms');
	}
}
