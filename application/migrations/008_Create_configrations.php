<?php
class Migration_Create_configrations extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field( array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'default' => 'Active'
			),
			'data' => array(
				'type' => 'TEXT',
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
		));
		
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('configrations');
	}

	public function down()
	{
		$this->dbforge->drop_table('configrations');
	}
}
