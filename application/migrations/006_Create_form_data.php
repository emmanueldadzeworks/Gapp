<?php
class Migration_Create_form_data extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'view_form_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'array_data' => array(
				'type' => 'TEXT',
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('form_data');
	}

	public function down()
	{
		$this->dbforge->drop_table('form_data');
	}
}
