<?php
class Migration_Create_navigation extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'order' => array(
				'type' => 'INT',
				'constraint' => '11',
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'default' => 'Non-Active'
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
			'body' => array(
				'type' => 'TEXT',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('navigation');
	}

	public function down()
	{
		$this->dbforge->drop_table('navigation');
	}
}