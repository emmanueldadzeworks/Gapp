<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forms extends Admin_Controller {
	
    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('navigation_m');
        $this->load->model('view_forms_m');
        


        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }
    public function index()
    {   
        $this->data['Navigation'] = $this->navigation_m->get();

        
        $this->data['content'] = 'Forms/home';
        $this->load->view('Control/Anatomy/Anatomy' , $this->data );
    }
    
    public function add_navigation($id = null)
    {   

        // Set up the form
		$rules = $this->navigation_m->rules;
		$this->form_validation->set_rules($rules);

        if ($this->form_validation->run() !== false) {
            $data                       = $this->input->post();
            $data['slug']               = underscore($this->input->post('title'));
            $data['status']             = "Active";

            if($this->navigation_m->save($data , $id)) {
                $this->data['notify'][] = array('title' => 'Success', 'message' => 'Request successfully sent', 'type' => 'success' );
                redirect(base_url('generic/forms'));
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => 'Error occurred', 'type' => 'error' );
            }
        }
        $this->index();
        
    }
    
    public function add_forms($id = null , $tb_name = null , $s_id = null)
    {   
        if ($this->input->post()) {
            $data['navigation_id']      = $id;
            $data['table_name']         = $tb_name;
            
            $data_forms                 = $this->input->post();
            $count = 0 ;
            foreach($data_forms[$tb_name] as $vaxx){
                $data_forms[$tb_name][$count]["lable"] = underscore($data_forms[$tb_name][$count]["lable"]);
                $count++;
            }      
            
            

            $data['array_form']         = json_encode($data_forms);

            if($this->view_forms_m->save($data , $s_id)) {
                $this->data['notify'][] = array('title' => 'Success', 'message' => 'Request successfully sent', 'type' => 'success' );
                redirect(base_url('generic/forms'));
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => 'Error occurred', 'type' => 'error' );
            }
        } else {
            $this->data['notify'][] = array('title' => 'Oops', 'message' => 'Validiation', 'type' => 'error' );
        }
        $this->index();
                
    }

}
