<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {
	
    function __construct()
    {
        parent::__construct();
        $this->load->model('form_data_m');
        $this->load->model('configrations_m');
        $this->load->model('navigation_m');


        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }
    
    public function index()
    {   
            
        $this->data['company_profile'] = $this->configrations_m->get_completed_percentage();
        $this->data['stored_data'] = count($this->form_data_m->get());
        $this->data['links_created'] = count($this->navigation_m->get());
        $this->data['users'] = count($this->user_m->get());
        
        
        $this->data['notify'][] = array('title' => 'Success', 'message' => 'Welcome to Generic application' , 'type' => 'success' );
        $this->data['content'] = 'Dashboard';
        $this->load->view('Control/Anatomy/Anatomy' , $this->data );
    }

}
