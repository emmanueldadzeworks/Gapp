<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller {

	private $_dashboard = 'generic/dashboard';
	
	function __construct() {
		parent::__construct();
		
	}		



	public function index ()
	{
		$this->data['users'] = $this->user_m->get();
		// Load the view
		$this->data['content'] = 'Users/Home';
        $this->load->view('Control/Anatomy/Anatomy' , $this->data );
	}
	public function edit ($id = NULL)
	{
		// Fetch a user or set a new one
		if ($id) {
			$this->data['user'] = $this->user_m->get($id);
			count($this->data['user']) || $this->data['errors'][] = 'User could not be found';
		}
		else {
			$this->data['user'] = $this->user_m->get_new();
		}
		
		// Set up the form
		$rules = $this->user_m->rules_admin;
		$id || $rules['password']['rules'] .= '|required';
		$this->form_validation->set_rules($rules);
		
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->user_m->dynamic_array_from_post();

			//photo upload and empty
			//Save the location of file in a array data
			$data = $this->database_location($data);

			
			$data['password'] = $this->user_m->hash($data['password']);

			$this->user_m->save($data, $id);
			$this->session->id == 1 ? redirect('cms/user') : redirect('cms/dashboard'); ;
		}
		// Load the view
		$this->data['content'] = 'User_creater';
		$this->load->view('cms_view/structure/structure_complete', $this->data);
		
		
	}

	public function User_previlages ($user_id = NULL)
	{

		$this->session->id == 1 ? "" : redirect('cms/dashboard'); ;
		// Fetch a User_previlages or set a new one
		if ($user_id) {
			$this->data['user'] = $this->user_m->get($user_id);
			$this->data['User_previlages'] = $this->User_privilege_m->get_by(array("user_id" => $user_id) , true);
			count($this->data['User_previlages']) || $this->data['errors'][] = 'User could not be found';
		}

		if (count($this->data['User_previlages']) > 0) {
			$id = $this->data['User_previlages']->id;
		} else {
			$id = null;
		}
		
		// Set up the form

		
			// Process the form
			if ($_POST) {
				$data['privilege'] = serialize($this->User_privilege_m->dynamic_array_from_post());
				$data['user_id'] = $user_id;

				if($this->User_privilege_m->save($data, $id))
				{
					redirect('cms/user');
				}
			}
			// Load the view
			$this->data['content'] = 'User_previlages';
			$this->load->view('cms_view/structure/structure_complete', $this->data);
	}

	public function delete ($id)
	{
		$this->user_m->delete($id);
		redirect('cms/user');
	}

	public function status ($id = NULL)
	{
		$Gear = $this->user_m->get($id);
		if ($Gear->status == 'Active') {
			$data['status'] = 'Non-Active';
		}
		else
		{
			$data['status'] = 'Active';
		}
		$this->user_m->save($data, $id);
		redirect('generic/users');
	}

}