<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configrations extends Admin_Controller {
	
    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('navigation_m');
        $this->load->model('configrations_m');
        


        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }
    public function index()
    {   
        $this->data['Navigation'] = $this->navigation_m->get();

        
        $this->data['content'] = 'Configrations/home';
        $this->load->view('Control/Anatomy/Anatomy' , $this->data );
    }
    
    public function logo($id = null)
    {   


        $title = "logo";
        $this->data[$title] = $this->configrations_m->get_by( array( "title" => $title) , true);

        if($this->data[$title]){
            $id = $this->data[$title]->id;
        } else {
            $id = null;
        }

        if ($_FILES ) {

            $image['logo'] = $this->database_location_file($_FILES , "_image");
            $image = array_merge( $image['logo'] , $this->file_contents($image['logo'] ));
            
            
            $data['data']               = json_encode( $image );
            $data['title']              = $title;

            if($this->configrations_m->save($data , $id)) {
				$this->session->set_flashdata('success', 'Request successfully sent');
            } else {
				$this->session->set_flashdata('error', 'Error occurred');
            }
            redirect('generic/configrations/logo' );
        }
        
        $this->data['content'] = 'Configrations/logo';
        $this->load->view('Control/Anatomy/Anatomy' , $this->data );
        
    }
    
    public function login_wallpaper($id = null)
    {   


        $title = "login_wallpaper";
        $this->data[$title] = $this->configrations_m->get_by( array( "title" => $title) , true);

        if($this->data[$title]){
            $id = $this->data[$title]->id;
        } else {
            $id = null;
        }

        if ($_FILES ) {

            $image['login_wallpaper'] = $this->database_location_file($_FILES , "_image");
            $image = array_merge( $image['login_wallpaper'] , $this->file_contents($image['login_wallpaper'] ));
            
            
            $data['data']               = json_encode( $image );
            $data['title']              = $title;

            if($this->configrations_m->save($data , $id)) {
				$this->session->set_flashdata('success', 'Request successfully sent');
            } else {
				$this->session->set_flashdata('error', 'Error occurred');
            }
            redirect('generic/configrations/login_wallpaper' );
        }
        
        $this->data['content'] = 'Configrations/login_wallpaper';
        $this->load->view('Control/Anatomy/Anatomy' , $this->data );
        
    }
    
    public function dashboard($id = null)
    {   

        // Set up the form
		$rules = $this->navigation_m->rules;
		$this->form_validation->set_rules($rules);

        if ($this->form_validation->run() !== false) {
            $data                       = $this->input->post();
            $data['slug']               = underscore($this->input->post('title'));
            $data['status']             = "Active";
            $data['status']             = "Active";

            if($this->navigation_m->save($data)) {
                $this->data['notify'][] = array('title' => 'Success', 'message' => 'Request successfully sent', 'type' => 'success' );
            } else {
                $this->data['notify'][] = array('title' => 'Oops', 'message' => 'Error occurred', 'type' => 'error' );
            }
        }

        
        $this->data['content'] = 'Configrations/dashboard';
        $this->load->view('Control/Anatomy/Anatomy' , $this->data );
        
    }
    
    public function properties($id = null)
    {   
        $title = "properties";
        $this->data[$title] = $this->configrations_m->get_by( array( "title" => $title) , true);

        if($this->data[$title]){
            $id = $this->data[$title]->id;
        } else {
            $id = null;
        }

        if ($this->input->post() ) {
            $data['data']               = json_encode($this->input->post());
            $data['title']             = $title;

            if($this->configrations_m->save($data , $id)) {
				$this->session->set_flashdata('success', 'Request successfully sent');
            } else {
				$this->session->set_flashdata('error', 'Error occurred');
            }
            redirect('generic/configrations/properties');
        }
        
        $this->data['content'] = 'Configrations/properties';
        $this->load->view('Control/Anatomy/Anatomy' , $this->data );
        
    }
    
}
