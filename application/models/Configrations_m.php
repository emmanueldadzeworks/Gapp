<?php
class Configrations_m extends MY_Model
{
	
	protected $_table_name = 'configrations';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';


	function __construct ()
	{
		parent::__construct();
	}

	function get_completed_percentage(){
		$count=0;
		foreach($this->get() as $value){
			$value->data !== "" ? $count++ : $count; 
		}
		return 25*$count;
	}

}