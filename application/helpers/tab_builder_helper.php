<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function form_tab_generator ($user)
{
    $CI =& get_instance();
    $count=0; 
    foreach (var_divition($user) as $key => $value) { ?>
        <div role="tabpanel" class="tab-pane fade <?php echo $count === 0 ? 'active in' : '' ; ?>" id="<?php echo $key; ?>">

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">

                        <div class="dropdown pull-right">
                            <!--
                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                            -->
                        </div>

                        <!--
                        <h4 class="header-title m-t-0 m-b-30"><?php echo _l($key); ?></h4>
                        -->

                        <div class="row">

                            <?php foreach ($value as $skey => $svalue) { ?>
                                <fieldset class="field-set-style">
                                    <legend><span style="color: #2666a6;     font-size: 28px;   font-weight: bold !important;"><?=humanize($skey)?></span></legend>
                                    <?php foreach ($svalue as $tkey => $tvalue) { ?>
                                            <div class="col-lg-5 boder-style" style="width: 46.179% !important;height: 70px !important;" >
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"><?php echo _l($tvalue['label']); ?></label>
                                                    <div class="col-md-8">
                                                        
                                                        <?php if(isset($tvalue['disabled'])){ 
                                                            $q=$tvalue['name']; 
                                                            if ($tvalue['input'] == "checkbox") {
                                                                if ($user->$q == true) {
                                                                    ?> <img style="width: 20px;" src="http://www.freeclipart.pw/uploads/correct-clip-art--vector-clip-art-online-royalty-free--public--13.png"> <?php
                                                                } else {
                                                                    ?> <img style="width: 20px;" src="http://images.clipartpanda.com/wrong-clipart-red-wrong-cross-md.png"> <?php
                                                                }
                                                            }
                                                            ?>
                                                            <?php 
                                                                $x= !empty($tvalue['dis_col']) ? $tvalue['dis_col'] : null ;  
                                                                echo isset($user->$q) ? strpos($q, '_id') ? $tvalue['dis']->$x: (isset($tvalue['nb_format']) ? number_format($user->$q , 2) : $user->$q) : "N/A"; 
                                                            ?>
                                                            <p><span id="<?php echo 'v_'.$tkey; ?>" style="color: #e867b0;"></span></p>
                                                        <?php } else { ?>
                                                        <a href="#<?php echo $tkey; ?>" id="C_<?=$tkey?>"  class="user-list-item waves-effect waves-light m-r-5 m-b-10" data-animation="fadein" data-plugin="custommodal"
                                                    data-overlaySpeed="200" data-overlayColor="#36404a">
                                                            <?php 
                                                            $q=$tvalue['name']; 
                                                            if ($tvalue['input'] == "checkbox") {
                                                                if ($user->$q == true) {
                                                                    ?> <img style="width: 20px;" src="http://www.freeclipart.pw/uploads/correct-clip-art--vector-clip-art-online-royalty-free--public--13.png"> <?php
                                                                } else {
                                                                    ?> <img style="width: 20px;" src="http://images.clipartpanda.com/wrong-clipart-red-wrong-cross-md.png"> <?php
                                                                }
                                                            }
                                                            ?>
                                                            <?php 
                                                                $x= !empty($tvalue['dis_col']) ? $tvalue['dis_col'] : null ;  
                                                                echo isset($user->$q) ? strpos($q, '_id') ? $tvalue['dis']->$x: $user->$q : "N/A"; 
                                                            ?>
                                                            <p><span id="<?php echo 'v_'.$tkey; ?>" style="color: #e867b0;"></span></p>
                                                        </a>
                                                        <?php }?>
                                                        
                                                    </div>
                                                    <!-- Modal -->
                                                    <?php if(!isset($tvalue['disabled'])){ ?>
                                                    <div id="<?php echo $tkey; ?>" class="modal-demo">
                                                        <button type="button" class="close" onclick="Custombox.close();">
                                                            <span class="submit_close">Submit</span>
                                                            <span class="sr-only">Close</span>
                                                        </button>
                                                        <h4 class="custom-modal-title"><?php echo _l('Request for change of '.$tvalue['label']); ?></h4>
                                                        <div class="custom-modal-text">
                                                            <p><?php 
                                                                echo _l('Current '.$tvalue['label'].':'); 
                                                                if (strpos($q, '_id')) {
                                                                    echo  $tvalue['dis']->$x;
                                                                } else {
                                                                    echo $user->$q;
                                                                }
                                                            ?> </p>
                                                            <p>
                                                                <?php 
                                                                    echo form_hidden('validation[]', $tvalue['name'].'/'.$tvalue['label'].'/'.$tvalue['validation']);
                                                                    switch ($tvalue['input']) {
                                                                        case 'textarea':
                                                                            echo '<textarea name="'.$tvalue['name'].'" class="form-control" rows="5" id="f_'.$tkey.'" onchange="document.getElementById(\'v_'.$tkey.'\').innerHTML = document.getElementById(\'f_'.$tkey.'\').value;"></textarea>';
                                                                            break;
                                                                        case 'date':
                                                                            echo '<input type="date" name="'.$tvalue['name'].'" class="form-control" id="f_'.$tkey.'" onchange="document.getElementById(\'v_'.$tkey.'\').innerHTML = document.getElementById(\'f_'.$tkey.'\').value;">';
                                                                            break;
                                                                        case 'checkbox':
                                                                            echo '<input type="checkbox" name="'.$tvalue['name'].'" value="1" class="form-control" id="f_'.$tkey.'" onchange="document.getElementById(\'v_'.$tkey.'\').innerHTML = document.getElementById(\'f_'.$tkey.'\').value;">'._l($tvalue['label']).'</input>';
                                                                            break;
                                                                        case 'pat':
                                                                            echo '<input type="text" pattern="'.$tvalue['pattern'].'" name="'.$tvalue['name'].'" class="form-control" id="f_'.$tkey.'" onchange="document.getElementById(\'v_'.$tkey.'\').innerHTML = document.getElementById(\'f_'.$tkey.'\').value;">'._l($tvalue['label']).'</input>';
                                                                            break;
                                                                        case "select":
                                                                            echo '<select class="form-control" name="'.$tvalue['name'].'" id="f_'.$tkey.'" onchange="document.getElementById(\'v_'.$tkey.'\').innerHTML = $(\'#f_'.$tkey.' :selected\').text();">';
                                                                                echo "<option></option>";
                                                                                foreach ($tvalue['select'] as $fkey => $fvalue) {
                                                                                    echo "<option value='".$fkey."'>".$fvalue."</option>";
                                                                                }
                                                                            echo '</select>';
                                                                            break;
                                                                        case "selectlist":
                                                                            echo '<select class="form-control" name="'.$tvalue['name'].'" id="f_'.$tkey.'" onchange="document.getElementById(\'v_'.$tkey.'\').innerHTML = $(\'#f_'.$tkey.' :selected\').text();">';
                                                                                echo "<option></option>";
                                                                                foreach (explode('|', $tvalue['list']) as $list) {
                                                                                    echo "<option value='".$list."'>".trim($list)."</option>";
                                                                                }
                                                                            echo '</select>';
                                                                                break;
                                                                        default:
                                                                            echo '<input type="'.$tvalue['input'].'" name="'.$tvalue['name'].'" class="form-control" id="f_'.$tkey.'" onchange="document.getElementById(\'v_'.$tkey.'\').innerHTML = document.getElementById(\'f_'.$tkey.'\').value;">';
                                                                            break;
                                                                    }
                                                                    if (isset($tvalue['ajax'])) {
                                                                        echo $tvalue['ajax'];
                                                                    }
                                                                ?>
                                                            </p>
                                                            <div class="dropify-wrapper" style="height: 200px;">
                                                            <div class="dropify-message">
                                                                <span class="file-icon"></span> 
                                                                <p>Drag and drop a file here or click</p>
                                                                <p class="dropify-error">Ooops, something wrong appended.</p>
                                                            </div>
                                                            <div class="dropify-loader"></div>
                                                            <div class="dropify-errors-container">
                                                                <ul></ul>
                                                            </div>
                                                            <input type="file" class="dropify" data-height="200" name="<?=$tvalue['name'].'_image'?>"><button type="button" class="dropify-clear">Remove</button>
                                                            <div class="dropify-preview">
                                                                <span class="dropify-render"></span>
                                                                <div class="dropify-infos">
                                                                    <div class="dropify-infos-inner">
                                                                        <p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>
                                                                        <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                    <?php } ?>
                                </fieldset>
                            <?php } ?>

                            <!--
                            <div class="col-lg-6">
                                <form class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="example-email">Email</label>
                                        <div class="col-md-10">
                                            <input type="email" id="example-email" name="example-email" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Password</label>
                                        <div class="col-md-8">
                                            <input type="password" class="form-control" value="password">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Placeholder</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="placeholder">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Text area</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>--><!-- end col -->

                            <!--<div class="col-lg-6">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Readonly</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" readonly="" value="Readonly value">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Disabled</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" disabled="" value="Disabled value">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Static control</label>
                                        <div class="col-sm-10">
                                          <p class="form-control-static">email@example.com</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Helping text</label>
                                        <div class="col-sm-10">
                                          <input type="text" class="form-control" placeholder="Helping text">
                                          <span class="help-block"><small>A block of help text that breaks onto a new line and may extend beyond one line.</small></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Input Select</label>
                                        <div class="col-sm-10">
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                            <h6>Multiple select</h6>
                                            <select multiple="" class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>--><!-- end col -->

                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
        </div>
        <?php $count++; ?>
    <?php } 
}