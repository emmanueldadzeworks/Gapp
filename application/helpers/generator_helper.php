<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function Generate_Password($length = 9, $add_dashes = false, $available_sets = 'luds')
{
	$sets = array();
	if(strpos($available_sets, 'l') !== false)
		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
	if(strpos($available_sets, 'u') !== false)
		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
	if(strpos($available_sets, 'd') !== false)
		$sets[] = '23456789';
	if(strpos($available_sets, 's') !== false)
		$sets[] = '!@#$%&*?';
	$all = '';
	$password = '';
	foreach($sets as $set)
	{
		$password .= $set[array_rand(str_split($set))];
		$all .= $set;
	}
	$all = str_split($all);
	for($i = 0; $i < $length - count($sets); $i++)
		$password .= $all[array_rand($all)];
	$password = str_shuffle($password);
	if(!$add_dashes)
		return $password;
	$dash_len = floor(sqrt($length));
	$dash_str = '';
	while(strlen($password) > $dash_len)
	{
		$dash_str .= substr($password, 0, $dash_len) . '-';
		$password = substr($password, $dash_len);
	}
	$dash_str .= $password;
	return $dash_str;
}

function valid_password($password = '')
	{
		$password = trim($password);
		$regex_lowercase = '/[a-z]/';
		$regex_uppercase = '/[A-Z]/';
		$regex_number = '/[0-9]/';
		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';
		if (empty($password))
		{
			$error[] = "Password is empty";
		}
		if (preg_match_all($regex_lowercase, $password) < 1)
		{
			$error[] = "Password must be at least one lowercase letter.";
		}
		if (preg_match_all($regex_uppercase, $password) < 1)
		{
			$error[] = "Password must be at least one uppercase letter";
		}
		if (preg_match_all($regex_number, $password) < 1)
		{
			$error[] = "Password must have at least one number.";
		}
		if (preg_match_all($regex_special, $password) < 1)
		{
			$error[] = "Password must have at least one special character." . " " . htmlentities('!@#$%^&*()\-_=+{};:,<.>§~') ;
		}
		if (strlen($password) < 5)
		{
			$error[] = "Password must be at least 5 characters in length.";
		}
		if (strlen($password) > 32)
		{
			$error[] = "Password cannot exceed 32 characters in length.";
		}
		return $error;
	}