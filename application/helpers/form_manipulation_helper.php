<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function d_submit ($pending)
{
    if ($pending > 0) { ?>
        <center>
            <h4 class="header-title m-t-0 m-b-30">You have a Pending Request</h4>
        </center>
    <?php } else { ?>
        <div class="row">
            <div class="form-group text-right m-b-0">
                <button class="btn btn-primary waves-effect waves-light" type="submit">
                    Submit
                </button>
                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                    Cancel
                </button>
            </div>
        </div>
    <?php }
}

function form_input_selector($input = null , $properties = array() , $db_values = null ) { ?>

    <div class="form-group">
    <label for="<?=underscore($properties->lable)?>"><?=$properties->lable?></label>
    <?php
        switch ($input) {
            case 'text':
                ?>
                    <input type="text" class="form-control" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" value="<?=$db_values?>" data-toggle="tooltip" data-placement="bottom" title="<?=$properties->lable?>" required>
                <?php
            break;
            case 'number':
                ?>
                    <input type="number" class="form-control" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" value="<?=$db_values?>" data-toggle="tooltip" data-placement="bottom" title="<?=$properties->lable?>" required>
                <?php
            break;
            
            case 'textarea':
                ?>
                    <textarea class="form-control" rows="4" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" data-toggle="tooltip" data-placement="bottom" title="<?=$properties->lable?>" required> <?=$db_values?> </textarea>
                <?php
            break;
            case 'password':
                ?>
                    <input type="password" class="form-control" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" required>
                <?php
            break;
            case 'select':
                ?>
                    <select class="form-control p-0" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" required>
                        <option></option>
                        <?php foreach(explode("|", $properties->select_array) as $key => $value){ ?>
                            <?php if(trim($value) === trim($db_values) ){ ?>
                                <option selected><?=trim($value)?></option>
                            <?php } else { ?>
                                <option><?=trim($value)?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                <?php
            break;
            case 'select_query':
                ?>
                    <select class="form-control p-0" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" required>
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                <?php
            break;
            
            default:
                ?><input type="text" class="form-control" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>"  value="<?=$db_values?>" data-toggle="tooltip" data-placement="bottom" title="<?=$properties->lable?>" required><?php
            break;
        } ?>
    </div>
    
    <?php
    
    
}


function forms_simplifier($form_view = null , $db_values = null ) { ?>
    <?=form_open_multipart(null , array("class" => "floating-labels")); ?>
        <?php
            foreach(json_decode($form_view->array_form) as $extract){
                foreach($extract as $key => $value){
                    $haskey = underscore($value->lable);
                    $show_db_values = $db_values === null ? "" : $db_values->$haskey;
                    form_input_selector($value->input_type , $value , $show_db_values );
                }
                break;
            }
            d_submit(0);
        ?>
    <?=form_close(); ?>


<?php } 


function data_table_headers($form_view = null ) { 
    foreach(json_decode($form_view->array_form) as $extract){
        foreach($extract as $key => $value){
            ?><th><?=$value->lable?></th><?php
        }
        break;
    }
} 

function data_table_headers_array($form_view = null ) { 
    $return=array();
    foreach(json_decode($form_view->array_form) as $extract){
        foreach($extract as $key => $value){
            $return[] = underscore($value->lable); 
        }
        break;
    }
    return (object) $return;
} 

function empty_string_for_null($string = null ) { 
    return $string == null ? "" : $string ; 
} 

function extract_lable( $objectx = null , $string ) { 
    $string = underscore($string);
    return  !isset($objectx->$string) ? "" : $objectx->$string ; 
} 

function prep_string_in_array($objectx = null ) { 
    return  !isset($objectx) ? "" : $objectx ; 
} 

